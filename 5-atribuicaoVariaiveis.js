console.log("trabalhando com atribuição de variaveis");


const primeiroNome = "Ricardo";
const sobreprimeiroNome = "Bugan";

console.log(primeiroNome + " " + sobreprimeiroNome);
console.log(`Meu primeiroNome é ${primeiroNome} ${sobreprimeiroNome}` );

//type error: atribui valor a uma constante se for const e dá serto se for let
//primeiroNome = primeiroNome + sobreprimeiroNome;
const primeiroNomeCompleto = primeiroNome + sobreprimeiroNome;

console.log(primeiroNomeCompleto);

//primeiroNomeCompleto = 2;
//console.log(primeiroNomeCompleto)

let contador = 0;
contador = contador + 1;


let idade;
idade = 26
idade = idade +1;

console.log(idade)